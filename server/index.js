const http = require('http');
const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');
const cors = require('cors');
var app = express();
var server = http.createServer(app);
//mongoose connect
mongoose.connect('mongodb://localhost:27017/testAuthenticated',function(err){
    if(err){
        console.error(err);
    }else{
        console.log('connected mongodb');
    }
})
// middleware
app.use(cors());
app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(session({
    secret : "secret",
    saveUninitialized: true,
    resave : true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use('/',require('./router'));
//
server.listen(4000,()=>{
    console.log("server is listenning!");
})