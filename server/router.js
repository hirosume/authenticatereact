var Route = require('express').Router();
var passport = require('passport');
var LocalStrategy = require('passport-local');
var bcrypt = require('bcrypt');
var db = require('./model');
// basic router

// db.create({username:'a',password:bcrypt.hashSync('a',bcrypt.genSaltSync(5))});
//
passport.serializeUser(function(user,done){
    done(null,user.id);
});
passport.deserializeUser((id,done)=>{
    db.findById(id).then(function(user){
        done(null,user);
    }).catch(function(err){
        console.log(err);
    });
});
passport.use(new LocalStrategy(function(username,password,done){
    db.findOne({username}).then((user)=>{

        bcrypt.compare(password,user.password,(err,result)=>{
            if(err){
                return done(err);
            }
            if(!result){
                done(null,false,{message:'Incorrect username or password'})
            }
            return done(null,user);
        });
        
    })
    .catch((err)=>{
        return done(err);
    })
}));

Route.post('/isLogin',(req,res)=>{
    if(!req.isAuthenticated()){
        res.send({isLogin:false});
    }
    else {
        res.send({isLogin:true,user:req.user});
    }
});
Route.post('/logout',(req,res)=>{
    req.logout();
    if(!req.isAuthenticated()){
        res.send({isLogin:false});
    }
    else {
        res.send({isLogin:true,user:req.user});
    }
})
Route.post('/login',passport.authenticate('local'),(req,res)=>{
    if(!req.isAuthenticated()){
        res.send({isLogin:false});
    }
    else {
        res.send({isLogin:true,user:req.user});
    }
});
module.exports = Route;