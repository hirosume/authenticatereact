import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
class App extends Component {
  constructor(props, context) {
    super(props, context);
  }
  state={
    isLogin:false,
    user:{name:"unknown"}
  }
  componentWillMount(){
    this.checkLogin();
  }
  
  async checkLogin(){
    let result = await axios.post('isLogin');
    this.setState(result.data);
  }
  async clickBtn(event){
    if(event.target.name=='login'){
      let result = await axios.post('/login',{username:'a',password:'a'});
      this.setState(result.data);
    }
    else{
      let result = await axios.post('/logout');
      this.setState(result.data);
    }
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <button name="login" onClick={this.clickBtn.bind(this)} disabled={this.state.isLogin}>{this.state.isLogin?"Logined":"Login"}</button>
        {(()=>{if(this.state.isLogin){return (<button name="logout" onClick={this.clickBtn.bind(this)}>Logout</button>)}})()}
      </div>
    );
  }
}

export default App;
